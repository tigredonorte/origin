Core = function(){
    this.hubs    = {};
    this.waitHub = {};
    this.i       = 0;
    this.extHub  = 'extension';
    this.quene   = {};
};

Core.prototype.hasHub = function(hubname){
    return(is_defined(hubname, this.hubs));
};

Core.prototype.register = function(hub, name, obj){
    console.log(name);
    console.log(obj);
    if(!this.hasHub(hub)){
        var sbox = "";
        if(this.extHub !== hub){
            if(!this.hasHub(this.extHub) || !this.hubs[this.extHub].has(hub)){
                if(!is_defined(hub, this.quene)){ this.quene[hub] = {}; }
                this.quene[hub] = new Core.hub(hub, sbox, h);
                this.quene[hub].add(name, obj);
                return;
            };
            sbox = this.hubs[this.extHub].get(hub);
        }
        var h          = is_defined(hub, this.waitHub)?this.waitHub[hub]:{};
        this.hubs[hub] = new Core.hub(hub, sbox, h);
        if(is_defined(hub, this.waitHub)) delete this.waitHub[hub];
    }
    this.hubs[hub].add(name, obj);
};

Core.prototype.load = function(hubname, classname, callback, ctx){
    if(!this.hasHub(hubname)) {
        if(!is_defined(hubname, this.waitHub)){ this.waitHub[hubname] = {};}
        if(!is_defined(classname, this.waitHub[hubname])){this.waitHub[hubname][classname] = {};}
        this.i++;
        var obj = {fn: callback, v:ctx};
        this.waitHub[hubname][classname][this.i] = obj;
        return;
    }
    var obj = this.hubs[hubname];
    obj.exec(classname, callback, ctx);
    return obj.get(classname);
};

Core.prototype.addHub = function(hubname, obj){
    this.register(this.extHub, hubname, obj);
    if(is_defined(hubname, this.quene)){
        this.quene[hubname].register(this);
        delete this.quene[hubname];
    }
};

core = new Core();