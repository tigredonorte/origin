Core.hub = function(hubname, sandbox, needInit){
    array         = {};
    quene         = {};
    this.hubname  = hubname;
    this.needInit = needInit;
    this.url      = '';
    this.obj      = null;
    this.sbox     = sandbox;
    //Core.call(this); //herança
};

Core.hub.prototype.isStarted = function(){
    return !is_null(this.obj);
};

Core.hub.prototype.setObject = function(obj){
    this.obj = obj;
};

Core.hub.prototype.has = function(name){
    return is_defined(name, array);
};

Core.hub.prototype.add = function(name, arr){
    if(this.has(name)){
        console.log("Atenção a classe "+ name + " já foi registrado no " + this.hubname +'!');
        return false;
    }
    array[name] = arr;
    if(!is_defined(name, this.needInit))return true;
    if(!this.isStarted()){
        quene[name] = name;
        return true;
    }
    for(var i in this.needInit[name]){
        try{
            var elm = this.needInit[name][i];
            if(is_function(elm.fn)){
                elm.fn(arr, elm.v);
            }
        }catch(e){console.log(e.message);}
    }
    delete this.needInit[name];
    return true;
};

Core.hub.prototype.rm = function(name){
    try{
        if(is_defined(name, array)){
            delete array[name];
        }
        return true;
    }catch(e){console.log(e.message); return false;}
};

Core.hub.prototype.get = function(name){
    if(!is_defined(name, array))return;
    return array[name];
};

Core.hub.prototype.exec = function(classname, callback, ctx){
    if(!is_function(callback)) return;
    var obj = this.get(classname);
    callback(obj, ctx);
};

Core.hub.prototype.dirBasic = function(){
    var url = document.URL.split('tests');
    return url[0];
};

Core.hub.prototype.register = function(core){
    for(var name in array){
        core.register(this.hubname, name, array[name]);
    }
};