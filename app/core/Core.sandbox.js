Core.Sandbox = function(){
    this.events   = {};
    this.ev_count = 0;
    
    this.extCount  = 0;
    this.onExtLoad = {};
    this.extensions = {};
};

Core.Sandbox.prototype.listen = function(event, action, context){
    if(!is_defined(event, this.events)){this.events[event] = {}}
    this.events[event][this.ev_count++] = new Core.Sandbox.Event(event, action, context);
};

Core.Sandbox.prototype.notify = function(data, event){
    if(!is_defined(event, this.events)){return;}
    for(var i in this.events[event]){
        try{
            this.events[event][i].notify(data);
        }catch(e){Core.print(e);};
    }
};

Core.Sandbox.prototype.log = function(str){
    console.log(var_dump(str));
};

Core.Sandbox.prototype.createBlock = function(data){
    try{
        return new Block(data);
    }catch(e){
        this.log('Erro ao criar um novo bloco! - ' + e.message);
    }
};

Core.Sandbox.prototype.extension = function(name, fn, ctx){
    try{
        if(is_defined(name, this.extensions)){
            var obj = this.extensions[name];
            fn(obj, ctx);
        }else{
            var obj = {'name': name, 'fn': fn, 'ctx': ctx};
            this.extCount++;
            this.onExtLoad[name][this.extCount] = obj;
        }
    }catch(e){
        this.log('Erro ao executar extensão - '+ name + ' ' + e.message);
    }
};

Core.Sandbox.prototype.setExtension = function(name, obj){
    
};