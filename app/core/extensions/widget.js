core.register('core', 'widget', {
    obj: null,
    container: null,
    init: function(name, widget){
        this.name      = name;
        this.started   = false;
        this.instance  = null;
        this.wselector = widget;
        this.obj       = null;
    }, 
            
    Selector: function(data_var){
        if(data_var === '') return this.wselector;
        return this.wselector.find("[data-var='" + data_var + "']");
    },
    
    get: function(data_var){
        var s = this.Selector(data_var);
        if(isInput(s)) return s.val();
        //console.log(data_var + ' not is input');
        return s.html();
    },
    
    update: function(data_var, value){
        var s = this.Selector(data_var);
        if(isInput(s)) {
            s.val(value);
            return;
        }
        //console.log(data_var + ' not is input');
        s.html(value);
    },
    
    del: function(data_var){
        this.Selector(data_var).fadeOut(1500, function(){
            $(this).remove();
        });
    },
    
    getAllDataVar: function(){
        var self = this;
        var obj  = {};
        this.Selector('').children('[data-var]').each(function(){
            if(isInput($(this))) {
                var name = $(this).attr('data-var');
                
            }
        });
    },
    
    event: function(event, data_var, callback, contx){
        this.Selector(data_var).on(event, function(ev){
            callback(ev, contx);
        });
    }
});