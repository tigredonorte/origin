core.register('core', 'dataObject', {
    obj: null,
    container: null,
    id: null,
    init: function(sb, obj, id){
        this.sb  = sb;
        this.obj = obj;
        this.id  = (is_def(id))?id:this.newId();
        this.obj.attr('data-id', this.id);
    },
            
    Selector: function(obj_var){
        if(obj_var === '') return null;
        return this.obj.find("[obj-var='" + obj_var + "']");
    },
           
    set: function(data){
        var self = this;
        for(var i in data){
            self.update(i, data[i]);
        }
    },
            
    get: function(obj_var){
        var s = this.Selector(obj_var);
        if(is_null(s)){return '';}
        if(isInput(s)) return s.val();
        //console.log(obj_var + ' not is input');
        return s.html();
    },
            
    update: function(obj_var, value){
        var s = this.Selector(obj_var);
        if(is_null(s)){return;}
        if(isInput(s)) {
            s.val(value);
            return;
        }
        s.html(value);
    }, 
            
    del: function(obj_var){
        this.sb.extension('effects', function(ext, elm){
            ext.remove(elm);
        }, this.Selector(obj_var));
    },
            
    getId: function(){
        return this.id;
    },
            
    newId: function(){
        return Math.floor((1 + Math.random()) * 0x10000).toString(16)+
           Math.floor((1 + Math.random()) * 0x10000).toString(16)+
           Math.floor((1 + Math.random()) * 0x10000).toString(16)+
           Math.floor((1 + Math.random()) * 0x10000).toString(16)+
           Math.floor((1 + Math.random()) * 0x10000).toString(16);
    }

});