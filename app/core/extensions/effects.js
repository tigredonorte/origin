core.register('core', 'effects', {
    obj: null,
    container: null,
    init: function(){}, 
    append: function(elm, data){
        elm.append(data);
    },
            
    prepend: function(elm, data){
        elm.preppend(data);
    },
   
    show: function(elm){
        elm.slideIn('slow');
    },

    hide: function(elm){
        elm.slideOut('slow');
    },
            
    remove: function(elm){
        elm.remove();
    }
});