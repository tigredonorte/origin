$import = function(src, callback, context){
    if(!is_def(this.list)) this.list = {};
    if(!is_defined(src, this.list)){
        this.list[src] = true;
        var scriptElem = document.createElement('script');
        scriptElem.setAttribute('src',src);
        scriptElem.setAttribute('type','text/javascript');
        document.getElementsByTagName('head')[0].appendChild(scriptElem);
    }
    if(is_function(callback))callback(context);
};

$importNoCache = function(src, callback, context){
    var ms = new Date().getTime().toString();
    var seed = "?" + ms;
    $import(src + seed, callback, context);
};