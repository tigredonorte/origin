/**
 * @class Blocks.text
 * @argument {object} block An object that must contain the name and type {line or paragrapth} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
core.register('blocks','text', {
    init: function(block, sbox){
        this.name       = "Texto";
        this.blpai      = new Blocks();
        this.block      = block;
        this.lineLenght = 64;
        this.options    = {
            type: {
                default:'line',
                options:{
                    line     :{'html':'input'   , 'type':'text', 'size': '64', 'name':'Uma linha de texto'},
                    paragraph:{'html':'textarea', 'name':'Área de texto grande'}
                 }
             }
        };
        this.results = new Core.result();
        this.defblc  = {type:'line', name:'Texto'};
        this.block   = merge_options(this.defblc, block);

        //valida o bloco
        if(!this.blpai.isValidBlock(this.block, this.name)) throw this.blpai.getResponse();
        if(typeof this.options.type.options[this.block.type] === 'undefined'){
            this.results.setStatus(false);
            this.results.setError("O bloco " + this.block.name + "(" + this.name + ") não possui o atributo type válido!\n\
            Os tipos válido são 'line' e 'paragraph'");
            throw this.results;
        }
        this.sbox = sbox;
    },
      
    /**
     * @function Blocks.text.getName
     * @returns {string} the name of this block
     */
    getName: function(){
        return this.name;
    },
            
   /**
    * @function Blocks.text.getResponse
    * @returns {Core.result} the result of an operation @see Core.result
    */
    getResponse: function(){
        return this.results;
    },
    
   /**
    * @function Blocks.text.validate
    * @argument {string} value Receive a string to be validated
    * @return {Bool} true if string is valid or false otherwise
    */
    validate: function(value){
        this.results.reset();
        //se não é uma string
        if(typeof value !== 'string') {
            this.results.setStatus(false);
            this.results.setError("A variável "+ this.block.name + " não é uma string válida!");
        }

        //se o atributo é do tipo linha e o tamanho da linha ultrapassou o tamanho máximo permitido
        else if(this.block.type === 'line' && value.length > this.lineLenght) {
            this.results.setStatus(false);
            this.results.setError("O atributo "+ this.block.name + " Ultrapassa o limite de "+this.lineLenght+" caracteres permitidos!");
        }
        return this.results.getStatus();
    },
            
    /**
     * @function Blocks.text.formulario
     * @argument {string} value Receive a string to be validated
     * @return {Bool} true if string is valid or false otherwise
     */
    formulario: function(elm, value){
        var element;
        var opt = this.options[this.block.type];
        element = document.createElement(opt['html']);
        delete opt['name'];
        delete opt['html'];
        for(var i in opt){
            element.setAttribute(i, opt[i]);
        }
        element.setAttribute('data-block', this.getName() );
        element.setAttribute('data-var'  , this.block.name);

        if(typeof value === 'undefined') value = "";
        element.value = value;
        elm.html(elm);
        return element;
    },
            
    format: function(value){
        if(!this.validate(value)) return false;
        return value;
    },
            
    /**
     * @function Blocks.password.getBlock
     * @returns {Object} An object with password rules
     */
    getBlock: function(){
        return this.block;
    },
            
    onInsert: function(value){},
    undoInsert: function(value){},
    onEdit: function(value){},
    undoEdit: function(value){},
    onDelete: function(value){},
    undoDelete: function(value){}
});