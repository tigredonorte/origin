/**
 * @class Blocks.email
 * @argument {object} block An object that must contain the name can contain invite option {true or false} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
core.register('blocks', 'email', {
    extensions: ['result'],
    init: function(block, sbox){
        this.definvite  = true;
        this.results    = new Core.result();
        this.name       = "Email";
        this.options    = {
            invite: {
                default:false,
                options:{true :{}, false:{}},
                desc:'Adicionar a esta lista e enviar convite para participar do sistema'
             }
        },
        this.defblc = {invite:true, name:'Email'},
        this.block = merge_options(this.defblc, block);

        //valida o bloco
        this.sb = sbox;
        //this.sb.isValidBlock(this.block, this.name);
    },
            
   /**
    * @function Blocks.email.getResponse
    * @returns {Core.result} the result of an operation @see Core.result
    */
    getResponse: function(){

    },
            
    /**
     * @function Blocks.email.getName
     * @returns {string} the name of this block
     */
    getName: function(){
        return this.name;
    },
            
   /**
    * @function Blocks.email.getName
    * @argument {string} value Receive a email to be validated
    * @return {Bool} true if email is valid or false otherwise
    */
    validate: function(value){
       this.results.reset();
       var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       if (!filter.test(value)) {
           this.results.setStatus(false);
           this.results.setError("O Email " + value +  " é inválido!");
       }
       return this.results.getStatus();
    },
            
    /**
     * @function Blocks.email.formulario
     * @argument {string} value Receive a 
     * @return {Bool} true if string is valid or false otherwise
     */
    formulario: function(value){
        if(!this.validate(value)) return false;
        if(typeof value === 'undefined') value = "";

        var element = document.createElement("input");
        element.setAttribute('type', 'text');
        element.setAttribute('data-block', this.getName());
        element.setAttribute('data-var'  , this.block.name);
        element.value = value;
        return element;
    },

    format: function(value){
        if(!this.validate(value)) return false;
        return value;
    },

    onInsert: function(value){},
    undoInsert: function(value){},
    onEdit: function(value){},
    undoEdit: function(value){},
    onDelete: function(value){},
    undoDelete: function(value){},
    getBlock: function(){return this.block; }
});
