/**
 * @class Blocks.list
 * @argument {object} block An object that must contain the name can contain invite option {true or false} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
core.register('blocks','list', {
    init: function(block, sbox){
        this.name       = "Email";
        this.options = {
            'choices_possible': {
                'default':'one',
                'options':{
                    one     :{name: 'Apenas uma escolha'},
                    limited :{name: 'Número limitado de escolhas'},
                    ilimited:{name: 'Sem limite de escolhas'}
                },
                'desc':'Número de escolhas possíveis'
             }
        };
        this.defblc = {choices_possible:'one'};
        this.block = merge_options(this.defblc, block);
        this.sbox = sbox;
    },
            
    getName: function(){
        return this.name;
    },
            
    getResponse: function(){},
    validate: function(value){},
    formulario: function(value){},
    format: function(value){},
    onInsert: function(value){},
    undoInsert: function(value){},
    onEdit: function(value){},
    undoEdit: function(value){},
    onDelete: function(value){},
    undoDelete: function(value){}
});