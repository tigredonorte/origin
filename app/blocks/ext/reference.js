core.register('blocks','reference', {
    init: function(block, sbox){
        this.results    = new Core.result();
        this.name       = "Senha";
        this.options = {
            plug: {desc:'Escolha o aplicativo referenciado'},
            type: {
                 default:'one',
                 options:{
                    one  :{name:'Simples'},
                    multi:{name:'Múltipla'}
                 },
                 desc:'A referência simples permite que apenas um item do aplicativo seja referenciado\n\
                       A múltipla permite que mais de um item do aplicativo seja referenciado.'
             }
        };
        this.defblc = {type:'one', name: this.name};
        this.block = merge_options(this.defblc, block);
        this.sbox = sbox;
    },
      
    /**
     * @function Blocks.password.getName
     * @returns {string} the name of this block
     */
    getName: function(){
        return this.name;
    },
            
    getResponse: function(){
        return this.results;
    },
    
    validate: function(value){
        return true;
    },
            
    formulario: function(value){
        
    },
            
    format: function(value){
        return value;
    },
            
    /**
     * @function Blocks.password.getBlock
     * @returns {Object} An object with password rules
     */
    getBlock: function(){
        return this.block;
    },
            
    onInsert: function(value){},
    undoInsert: function(value){},
    onEdit: function(value){},
    undoEdit: function(value){},
    onDelete: function(value){},
    undoDelete: function(value){},
});