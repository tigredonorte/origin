<?php

class findTests{
    
    public function __construct() {
        $this->dir = dirname(__FILE__);
    }
    
    private $arquivos = array();
    private $pastas   = array();
    private function findFiles($diretorio){

        //apaga os arquivos e pastas, se ja foram setados
        $this->arquivos = $this->pastas = array();

        // abre o diretório
        if(!file_exists($diretorio)) return array();
        $ponteiro  = opendir($diretorio);

        // monta os vetores com os itens encontrados na pasta
        while ($listar = readdir($ponteiro)){
            if ($listar!="." && $listar!=".." && $listar!=".DS_Store"){
                if (is_dir($diretorio . "/".$listar)) $this->pastas[] = $listar;
                elseif(is_file($diretorio . "/".$listar)) $this->arquivos[] = $listar;
            }
        }
    }

    private function getPastas($diretorio = ""){
        if($diretorio != "") $this->findFiles($diretorio);
        return $this->pastas;
    }

    private function getArquivos($diretorio = ""){
        if($diretorio != "") $this->findFiles($diretorio);
        return $this->arquivos;
    }
    
    private $tests = array();
    private $prefix = '.test.js';
    private function find($dir, $folder = ""){
        $pastas   = $this->getPastas($dir);
        $arquivos = $this->getArquivos();
        if(!empty($arquivos)){
            foreach($arquivos as $arq){
                if(array_key_exists($arq, $this->blacklist)) continue;
                if(strstr($arq, $this->prefix) !== false){
                    $this->tests[] = "$folder/$arq";
                }
            }
        }
        foreach($pastas as $p){
            if(array_key_exists($p, $this->blacklist)) continue;
            $f = ($folder == "")?$p: "$folder/$p";
            $this->find("$dir/$p", $f);
        }
    }
    
    public function getTests($initial_folder = ""){
        $this->find($this->dir, $initial_folder);
        return $this->tests;
    }
    
    public function reset(){
        $this->arquivos = $this->pastas = $this->tests = array();
    }
    
    public function setPrefix($prefix){
        $this->prefix = $prefix;
    }
    
    public function setDir($dir){
        $this->dir = $dir;
    }
    
    private $blacklist = array();
    public function add2blacklist($name){
        $this->blacklist[$name] = $name;
    }
    
    public function genKey($num){
        $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $key = '';
        $max = strlen($str) - 1;
        for($i=0;$i<$num;$i++)
            $key .= substr($str,rand(0, $max),1);
        return $key;
    }
    
    public function printScripts($dir, $genKey = true){
        $k = $this->genKey(4);
        if(empty($this->tests)) $this->getTests();
        $js = $this->tests;
        $link = URL."{$dir}/";
        $url = ($genKey)?"?v=$k":"";
        foreach($js as $j){
            echo "<script type='text/javascript' src='{$link}{$j}{$url}'></script>";
        }
    }
}

?>