<?
require_once '../defines.php';
?>
<html>
<head>
  <meta charset="utf-8">
  <title>QUnit Example</title>
  <link rel="stylesheet" href="src/qunity.css">
</head>
<body>
  <div id="qunit"></div>
  <div id="qunit-fixture"></div>
  <script src="src/qunity.js"></script>
  <script src="../app/utils/phpjs.js"></script>
  <script src="../app1/lib/jquery.js"></script>
  <?php

    require_once './findtests.php';
    
    $ft2 = new findTests();
    $ft2->setDir(DIR_BASIC."/app");
    $ft2->setPrefix('.js');
    $ft2->add2blacklist('utils');
    $ft2->printScripts("app");
    
    $ft = new findTests();
    $ft->setDir(dirname(__FILE__)."/tests");
    $ft->setPrefix('.test.js');
    $ft->printScripts("tests/tests");

    
    ?>
</body>
</html>