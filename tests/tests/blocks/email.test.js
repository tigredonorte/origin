core.load('blocks', 'email', function(email){
    test('Blocks.email Sanity', function() {
        assertNotNull(email, "O bloco email não pode ser nulo!");
        assertObject(email, "O bloco email não existe ou não foi declarado corretamente!");
    });
    
    test('Blocks.email Validate', function() {
        email.init({invite: false});
        assertFalse(email.validate({}));
        assertFalse(email.validate(123456));
        assertFalse(email.validate([]));
        assertFalse(email.validate(''));
        assertFalse(email.validate('email'));
        assertFalse(email.validate('email@'));
        assertFalse(email.validate('email@email'));
        assertTrue(email.validate('email@email.com'));
        assertTrue(email.validate('email@email.com.pl'));
    });
});
