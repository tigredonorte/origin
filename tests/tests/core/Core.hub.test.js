test('Core.hub Sanity', function() {
    var eclas = Core.hub;
    var obj   = new eclas('myhub', 'myurl');
    assertObject(obj);
    assertInstanceOf(eclas, obj);
});

test('Core.hub add & remove', function() {
    var testname = 'test';
    var eclas = Core.hub;
    var obj   = new eclas('myhub', {}, 'myurl');
    assertFalse(obj.has(testname));
    assertUndefined(obj.get(testname));
    assertTrue(obj.add(testname, {'obj':'a'}));
    assertObject(obj.get(testname));
    assertFalse(obj.add(testname, {'obj':'b'}));
    assertTrue(obj.has(testname));
    assertTrue(obj.rm(testname));
    assertFalse(obj.has(testname));
    assertUndefined(obj.get(testname));
});