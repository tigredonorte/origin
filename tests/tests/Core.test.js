test('Core - Sanity', function() {
    var eclas = Core;
    var obj   = new eclas();
    assertObject(obj);
    assertInstanceOf(eclas, obj);
});

test('Core - Using hub Test', function() {
    var eclas = Core;
    var obj   = new eclas();
    
    assertUndefined(obj.load('myhub', 'myobj'), "Objeto myobj ainda não declarado");
    assertFalse(obj.hasHub('myhub'), "Objeto myobj ainda não declarado");
    
    obj.register('myhub', 'myobj', {
        init: function(){
            return "a";
        }
    });
    obj.addHub('myhub', {init:function(){return 'a';}});
    assertTrue(obj.hasHub('myhub'), "Hub myhub não foi adicionado corretamente!");
    var myobj = obj.load('myhub', 'myobj');
    //console.log(myobj);
    assertEqual(myobj.init(), 'a', "Objeto myhub.myobj deve retornar a string 'a' no método init");
});