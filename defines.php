<?php

if (!defined('DIR_BASIC')){
    define('DIR_BASIC', realpath(dirname(__FILE__))."/");
}

//nome do diretorio onde estara o projeto
if(!defined("PROJECT")){
    $dir   = str_replace("\\", "/", DIR_BASIC);
    $doc   = str_replace("\\", "/", $_SERVER['DOCUMENT_ROOT']);
    $pj_name = str_replace(array($doc, "config", "//", 'defines'), array("", "", "/", ""), $dir);
    define('PROJECT', $pj_name);
}

if (!defined('URL')){
    define('URL', "http://".$_SERVER['HTTP_HOST'] .PROJECT);
}
?>