<?php

require_once './plugins/invoice/invoice.html';

?>
<script type='text/javascript' src='http://projetos/hat/static/js/jquery-latest.min_1.js'></script>
<script type="text/javascript" src="utils/phpjs.js"></script>
<script type="text/javascript" src="utils/loader.js"></script>
<script type="text/javascript" src="utils/object.js"></script>
<script type="text/javascript" src="core/Core.js"></script>
<script type="text/javascript" src="core/extensions/Core.sandbox.js"></script>
<script type="text/javascript" src="core/extensions/Core.widget.js"></script>
<script type="text/javascript" src="core/extensions/Core.dataObject.js"></script>
<script type="text/javascript" src="core/extensions/Core.effects.js"></script>
<script type="text/javascript" src="core/sandbox/Event.js"></script>
<script>

//core.importExtension('sandbox');
//core.importExtension('widget');
//core.importExtension('dataObject');
//core.importExtension('effects');

core.findWidgets();
$importNoCache('plugins/invoice/in_form.js');
$importNoCache('plugins/invoice/in_div.js');
$importNoCache('plugins/invoice/in_sum.js');
$importNoCache('plugins/invoice/in_table.js');

$(document).ready(function(){
    core.createInstance('in_form' , 'widget1');
    core.createInstance('in_div'  , 'widget2');
    core.createInstance('in_sum'  , 'widget3');
    core.createInstance('in_table', 'widget4');
});

</script>

<body>
    <div id="all">
        <div id="widget1"></div>
        <div id="widget2"></div>
        <div id="widget3"></div>
        <div id="widget4"></div>
    </div>
</body>