Core = function(){
    this.config = {
        'target-widget'     : 'div[data-widget]',
        'target-widget-name': 'widget'
    };
    this.importCount = 0;
    this.isReady     = false;
    
    this.extensions = {};
    this.ext_callbk = {};
    this.plugins    = {
        w: {},
        plugs: {},
        isValid: function(name, obj){
            if(!is_object(obj)){
                console.log("O widget " + name + " não é um objeto!");
                return false;
            }
            if(!method_exists(obj, 'init')){
                console.log("O método init não existe no widget " + name);
                return false;
            }
            return true;
        }, 
        add: function(name, obj, html){
            if(!this.isValid(name, obj)) return false;
            this.plugs[name] = obj;
            this.w[name]     = html;
            return true;
        },
        rm: function(name){
            try{
                if(is_defined(name, this.plugs)){
                    delete this.plugs[name];
                    delete this.w[name];
                    return true;
                }
                return false;
            }catch(e){console.log(e.message); return false;}
        }
    };
    this.instances  = {
        inst: {},
        has: function(name){},
        add: function(name){},
        rm: function(name){},
        get: function(name){},
        start: function(){},
        foo: function(name){
            if(is_defined(name, this.inst)){
                for(var i in this.inst[name]){
                    this.instantiate(this.inst[name][i]);
                }
            }
        }
    };
    
    this.widgets    = {
        widgets: {},
        find: function(target, targetName){
            var self = this;
            $(target).each(function(){
                try{
                    var wname = $(this).data(targetName);
                    var clone = $(this).clone(true, true);
                    $(this).remove();
                    self.widgets[wname] = clone;
                }catch(e){console.log(JSON.stringify(e));}
            });
        }
    };
    
};

Core.prototype.findWidgets = function(){
    this.widgets.find(this.config['target-widget'], this.config['target-widget-name']);
    this.sbox = new Core.Sandbox();
};

Core.prototype.register = function(name, obj){  
    this.instances.started(name);
};

/**
 * Associa um widget a um elemento do navegador, caso o widget não tenha sido 
 * registrado ainda, ele é colocado na fila de instanciação e será instanciado
 * no momento que o widget for registrado.
 * 
 * @param {string} widgetName   - Nome do widget (marcadores com data-widget)
 * @param {string} instanceName - Id do local onde o widget será criado
 */
Core.prototype.createInstance = function(widgetName, instanceName){
    this.loadExtension('widget', function(ext, self){
        ext.init(widgetName, instanceName);
        if(!is_defined(widgetName, self.instances)) self.instances[widgetName] = {};
        self.instances[widgetName][instanceName] = ext;
        if(!is_defined(widgetName, self.plugins)){return;}
        self.instantiate(ext);
    }, this);
    
};

/**
 * Inicializa o novo widget
 * @param {Core.widget} inst
 */
Core.prototype.instantiate = function(inst){
    var self         = this;
    try{
        $(inst.wselector).html(self.widgets[inst.name]);
        console.log(JSON.stringify(inst));
        var obj       = self.plugins[inst.name];     
        var name      = inst.wid;
        var i         = inst.name;
        var widget    = this.instances[i][name];
        try{
            obj.init(this.sbox, widget);
            inst.obj                = obj;
            inst.started            = true;
            
            if(!is_defined(name, this.instances)) this.instances[name] = {};
            this.instances[name][i] = inst;
        }catch(e){
            self.print("Erro ao instanciar widget " + i + '\n' + e.message);
        }
        
    }
    catch(e){
        console.log('O widget '+ inst.name + ' não pôde ser instanciado' + JSON.stringify(e) + '\n'+e.message); 
        return;
    }
};

/**
 * 
 * @param {string} extName
 * @param {function} fn
 * @param {mixed} ctx
 */
Core.prototype.loadExtension = function(extName, fn, ctx){
    try{
        if(!is_defined(extName, this.extensions)){
            this.addExtCallback(extName, fn, ctx);
            return;
        }
        var obj = this.extensions[extName];
        fn(obj, ctx);
    }catch(e){console.log(e.message);}
};

Core.prototype.registerExtension = function(extName, obj){
    if(typeof this.extensions[extName] !== 'undefined'){throw {'erro':'A extensão '+extName+' Já foi declarada'}}
    this.extensions[extName] = obj;
    this.ExecExtCallback(extName);
};

Core.prototype.addExtCallback = function(extName, fn, ctx){
    if(is_def(this.extCount)) this.extCount = 0;
    this.ext_callbk[extName][this.extCount] = {'extName': extName, 'fn': fn, 'ctx': ctx};
    this.extCount++;
};

Core.prototype.getExtCallback = function(extName){
    if(!is_defined(extName, this.ext_callbk)) this.ext_callbk[extName] = {};
    var ret = this.ext_callbk[extName];
    this.ext_callbk[extName] = {};
    return ret;
};

Core.prototype.ExecExtCallback = function(extName){
    if(!is_defined(extName, this.ext_callbk)) return;
    var exts = this.ext_callbk[extName];
    var current_ext = this.extensions[extName];
    for(var i in exts){
        var fn  = exts[i]['fn'];
        var ctx = exts[i]['ctx'];
        fn(current_ext, ctx);
    }
    //{extName: extName, fn: fn, ctx: ctx}
};

Core.prototype.importExtension = function(extName){
    var file = extName+'.js';
    this.importCount++;
    this.isReady = false;
    $import(file, function(self){
        self.importCount--;
        if(self.importCount === 0){
            self.isReady = true;
        }
    }, this);
};

var core = new Core();