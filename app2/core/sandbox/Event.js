Core.Sandbox.Event = function(name, action, context){
    this.name    = name;
    this.action  = action;
    this.context = context;
};

Core.Sandbox.Event.prototype.notify = function(data){
    this.action(data, this.context);
};