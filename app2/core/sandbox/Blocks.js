Core.Sandbox.Blocks = function(){
    this.blocks = {};
    this.callback = {};
};

Core.Sandbox.Blocks.prototype.addBlock = function(name, fn){
    core.print(name);
    if(!is_object(fn)){throw {message: 'Erro ao adicionar o bloco ' + name}}
    this.blocks[name] = fn;
    this.execCallback(name);
};

/**
 * Retorna um novo bloco
 * @param {string} name Nome do bloco
 * @param {function} fn Função de callback (caso o bloco não tenha sido instanciado ainda)
 * @param {mixed} ctx contexto da função de callback
 * @returns {@exp;@call;is_defined}
 */
Core.Sandbox.Blocks.prototype.getBlock = function(name, fn, ctx){
    if(!is_defined(name, this.blocks)){
        if(!is_defined(name, this.callback)) this.callback[name] = {i:0};
        this.callback[name][this.callback[name].i++] = {fn: fn, ctx: ctx};
        return;
    }
    
    this.execCallback(name);
    return clone(this.blocks[name]);
};

/**
 * Executa os callbacks armazenados
 */
Core.Sandbox.Blocks.prototype.execCallback = function(nm){
    for(var i in this.callback[nm]){
        var obj = clone(this.blocks[name]);
        this.callback[nm][i].fn(this.callback[nm][i].ctx, obj);
    }
    delete this.callback[nm];
};