<?php
require_once '../defines.php';
class loader{
    
    public function __construct() {
        $this->dir = dirname(__FILE__);
    }
    
    private $arquivos = array();
    private $pastas   = array();
    private function findFiles($diretorio){

        //apaga os arquivos e pastas, se ja foram setados
        $this->arquivos = $this->pastas = array();

        // abre o diretório
        if(!file_exists($diretorio)) return array();
        $ponteiro  = opendir($diretorio);

        // monta os vetores com os itens encontrados na pasta
        while ($listar = readdir($ponteiro)){
            if ($listar!="." && $listar!=".." && $listar!=".DS_Store"){
                if (is_dir($diretorio . "/".$listar)) $this->pastas[] = $listar;
                elseif(is_file($diretorio . "/".$listar)) $this->arquivos[] = $listar;
            }
        }
    }

    private function getPastas($diretorio = ""){
        if($diretorio != "") $this->findFiles($diretorio);
        return $this->pastas;
    }

    private function getArquivos($diretorio = ""){
        if($diretorio != "") $this->findFiles($diretorio);
        return $this->arquivos;
    }
    
    private $tests = array();
    private $prefix = '.js';
    private function find($dir, $folder = ""){
        $pastas   = $this->getPastas($dir);
        $arquivos = $this->getArquivos();
        if(!empty($arquivos)){
            foreach($arquivos as $arq){
                if(strstr($arq, $this->prefix) !== false){
                    $this->tests[] = "$folder/$arq";
                }
            }
        }
        foreach($pastas as $p){
            $f = ($folder == "")?$p: "$folder/$p";
            $this->find("$dir/$p", $f);
        }
    }
    
    public function printScripts($dir){
        if(empty($this->tests)) $this->find($dir);
        $js = $this->tests;
        $link = URL."{$dir}/";
        foreach($js as $j){
            echo "<script type='text/javascript' src='{$link}{$j}}'></script>";
        }
    }
}

?>