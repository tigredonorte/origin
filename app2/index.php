<?php

require_once './plugins/account/account.html';

?>
<script type='text/javascript' src='http://projetos/hat/static/js/jquery-latest.min_1.js'></script>
<script type="text/javascript" src="utils/phpjs.js"></script>
<script type="text/javascript" src="utils/loader.js"></script>
<script type="text/javascript" src="utils/object.js"></script>
<script type="text/javascript" src="core/Core.js"></script>
<script type="text/javascript" src="core/Core.sandbox.js"></script>
<script type="text/javascript" src="core/Core.widget.js"></script>
<script type="text/javascript" src="core/sandbox/Blocks.js"></script>
<script type="text/javascript" src="core/sandbox/Event.js"></script>
<script type="text/javascript" src="blocks/Block.js"></script>
<script>

core.findWidgets();
$importNoCache('plugins/account/ac01.js');
$importNoCache('plugins/account/ac02.js');
$importNoCache('plugins/account/ac03.js');
$importNoCache('plugins/account/ac04.js');

$(document).ready(function(){
    core.createInstance('acc01', 'widget1');
    core.createInstance('acc02', 'widget2');
    core.createInstance('acc03', 'widget3');
    core.createInstance('acc04', 'widget4');
    //core.createInstance('acc01', 'widget1again');
});

</script>

<body>
    
    <div id="all">
        <div id="widget1"></div>
        <div id="widget2"></div>
        <div id="widget3"></div>
        <div id="widget4"></div>
        <div id="widget1again"></div>
    </div>
    
</body>