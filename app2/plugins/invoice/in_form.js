core.register('in_form', {
    sb: null,
    widget: null,
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
      
        this.add();
    },
    
    add: function(){        
        this.widget.event('click', 'send', function(ev, self){
            var dv = self.widget.getAllDataVar();
            if(is_defined('send', dv)) {delete(dv['send']);}
            self.widget.resetData();
            self.sb.notify(dv, 'in_form_add');
        }, this);
    }
});