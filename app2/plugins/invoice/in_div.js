core.register('in_div', {
    sb: null,
    widget: null,
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
        this.inform_add();
    },
    
    inform_add: function(){
        this.sb.listen('in_form_add', function(data, self){
            console.log(JSON.stringify(data));
        }, this);
    }
});