core.register('acc01', {
    sb: null,
    widget: null,
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
        this.r      = 'res';
        this.text   = 'Apple';
        this.sb.listen('test2', function(data, self){
            self.widget.append(self.r,data + "\n");
        }, this);
        
        this.sb.listen('test3', function(data, self){
            self.widget.append(self.r,data + "\n");
        }, this);
        
        this.widget.event('click', 'clear', function(ev, self){
            self.widget.update(self.r,"");
            self.sb.notify(self.text, 'test1_clear');
        }, this);
        
        this.widget.event('click', 'clickme', function(ev, self){
            self.sb.notify(self.text, 'test1');
        }, this);
        
        
        //this.sb.notify('acc01', 'test1');
        //this.onAdd();
        //this.onUpdate();
        //this.onDelete();
    },
    
    onAdd: function(){
        this.sb.listen('acc/account/create', function(data, self){
            self.widget.addObject(data);
        }, this);
    },
    
    onUpdate: function(){
        this.sb.listen('acc/account/update', function(data, self){
            self.widget.addObject(data.id, data);
        }, this);
    },
    
    onDelete: function(){
        this.sb.listen('acc/account/delete', function(data, self){
            self.widget.deleteObject(data.id, data);
        }, this);
    }
});