core.register('acc02', {
    sb: null,
    widget: null,
    soma: 0,
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
        this.r      = 'res';
        this.sb.listen('test1', function(data, self){
            self.widget.append(self.r, data + "\n");
        }, this);
        this.sb.listen('test3', function(data, self){
            self.widget.append(self.r, data + "\n");
        }, this);
        
        this.widget.event('click', 'clear', function(ev, self){
            self.widget.update(self.r,"");
        }, this);
        
        this.widget.event('click', 'clickme', function(ev, self){
            self.sb.notify('Banana', 'test2');
        }, this);
        //this.onAdd();
        //this.onUpdate();
        //this.onDelete();
        //this.widget.update('soma', this.soma);
    },
    
    onAdd: function(){
        this.sb.listen('acc/account/create', function(data, self){
            var sum   = self.widget.get('sum') + data.amount;
            self.widget.update('sum', sum);
            self.sb.notify(sum, 'acc/account/total/update');
        }, this);
    },
    
    onDelete: function(){
        this.sb.listen('acc/account/delete', function(data, self){
            var sum   = self.widget.get('sum') - data.amount;
            self.widget.update('sum', sum);
            self.sb.notify(sum, 'acc/account/total/update');
        }, this);
    },
            
    onUpdate: function(){
        this.sb.listen('acc/invoice/add', function(data, self){
            var total = self.widget.get('sum')+ data.price;
            self.widget.update('sum', total );
            self.sb.notify(total, 'acc/account/total/update');
        }, this);
        
        this.sb.listen('acc/invoice/remove', function(data, self){
            var total = self.widget.get('sum') - data.price;
            self.widget.update('sum', total);
            self.sb.notify(total, 'acc/account/total/update');
        }, this);
        
        this.sb.listen('acc/invoice/update', function(data, self){
            var total = self.widget.get('sum');
            var val   = data.price - data.__old.price;
            self.widget.update('sum', total + val);
            self.sb.notify(total + val, 'acc/account/total/update');
        }, this);
    }
});