/*
 * app: acc/account
 */
core.register('acc03', {
    sb: null,
    widget: null,
    bl: null,
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
        this.r      = 'res';
        this.last   = '';
        this.sb.listen('test1', function(data, self){
            self.received(data);
        }, this);
        
        this.sb.listen('test2', function(data, self){
            self.received(data);
        }, this);
        
        this.sb.listen('test1_clear', function(data, self){
            self.clear(data);
        }, this);
        
        this.sb.listen('test2_clear', function(data, self){
            self.clear(data);
        }, this);
        //this.bl     = this.sb.createBlock(this.data); 
    },
            
    received: function(data){
        this.last = data;
        this.widget.update(this.r,'i received '  +data);
    },
            
    clear: function(data){
        if(this.last === data){
            this.widget.update(this.r,'the data is cleared ');
            this.last = data;
        }
    },
            
    data: {
        'nome'  :{'name':'Nome'  , 'block':'text'     , 'req':true, 'desc': 'Digite o seu nome completo'},
        'email' :{'name':'Email' , 'block':'email'    , 'req':true, 'desc': 'Digite o email de login'},
        'senha' :{'name':'Senha' , 'block':'password' , 'req':true, 'desc': 'Digite sua senha'},
        //'perfil':{'name':'Perfil', 'block':'reference', 'req':true, 'ref':{'plug':'usuario.perfil', 'type':'one'}}
    }
});