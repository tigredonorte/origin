/*
 * app: acc/account
 */
core.register('acc04', {
    sb    : null,
    widget: null,
    total : {},
    init: function(sandbox, widget){
        this.sb     = sandbox;
        this.widget = widget;
        this.sb.listen('test1', function(data, self){
            self.updateTest('test1', data);
        }, this);
        
        this.sb.listen('test2', function(data, self){
            self.updateTest('test2', data);
        }, this);
        
        this.sb.listen('test1_clear', function(data, self){
            self.clear('test1', data);
        }, this);
        
        this.sb.listen('test2_clear', function(data, self){
            self.clear('test2', data);
        }, this);
    },
    
    updateTest: function(test, data){
        if(!is_defined(test, this.total)){
            this.total[test] = 0;
            this.widget.add(test, data + ": " + this.total[test]);
        }
        this.total[test]++;
        this.widget.update(test,data + ": " +this.total[test]);
    },
     
    clear: function(test, data){
        this.total[test] = 0;
        this.widget.update(test, data + ": " + this.total[test]);
    }
});