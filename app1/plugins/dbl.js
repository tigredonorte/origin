Plugin.dbl = function(){
    this.title = '';
    this.data = {};
    this.blocks = {};
};

Plugin.dbl.prototype.init = function(sandbox){
    this.sandbox = sandbox;
    for(var i in this.data){
        this.blocks[i] = this.sandbox.blocks.init(i, this.data[i]);
    }
};

Plugin.dbl.prototype.getData = function(){
    return this.data;
};
Plugin.dbl.prototype.getBlock = function(){
    return this.blocks;
};

Plugin.dbl.prototype.validate = function(dados){
    var results = {};
    for(var i in this.blocks){
        if(!dados.hasOwnProperty(i)) dados[i] = "";
        var res = this.validateField(i, dados[i]);
        if(!res.valid) results[i] = res;
    }
    return results;
};

Plugin.dbl.prototype.validateField = function(field, dado){
    return this.blocks[field].validate(dado);
};