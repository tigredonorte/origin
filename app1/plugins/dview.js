Plugin.dview = function(sandbox, model, blocks){
    this.sbx = sandbox;
    this.md  = model;
    this.bl  = blocks;
};

Plugin.dview.prototype.index = function(){
    var dtype = this.md.getName();
    var th    = '';
    var tb    = '';
    for(var i in this.bl){
        th += "<td data-block='"+this.bl[i].getName()+"'></td>";
        tb += "<td data-block='"+this.bl[i].getName()+"' data-var='"+i+"'></td>";
    }
    return "<table data-type='"+dtype+"'>\n\
                <thead>\n\
                    <tr>"+th+"</tr>\n\
                </thead>\n\
                <tbody>\n\
                    <tr data-var='__key'>"+tb+"</tr>\n\
                </tbody>\n\
            </table>";
};