core.register('acc02', {
    sb: null,
    widget: null,
    init: function(sandbox){
        this.sb     = sandbox;
        this.widget = this.sb.getWidget('acc02');
        this.onAdd();
        this.onUpdate();
        this.onDelete();
    },
    
    onAdd: function(){
        this.sb.listen('acc/account/create', function(data, self){
            var sum   = self.widget.get('sum') + data.amount;
            self.widget.update('sum', sum);
            self.sb.notify(sum, 'acc/account/total/update');
        }, this);
    },
    
    onDelete: function(){
        this.sb.listen('acc/account/delete', function(data, self){
            var sum   = self.widget.get('sum') - data.amount;
            self.widget.update('sum', sum);
            self.sb.notify(sum, 'acc/account/total/update');
        }, this);
    },
            
    onUpdate: function(){
        this.sb.listen('acc/invoice/add', function(data, self){
            var total = self.widget.get('sum')+ data.price;
            self.widget.update('sum', total );
            self.sb.notify(total, 'acc/account/total/update');
        }, this);
        
        this.sb.listen('acc/invoice/remove', function(data, self){
            var total = self.widget.get('sum') - data.price;
            self.widget.update('sum', total);
            self.sb.notify(total, 'acc/account/total/update');
        }, this);
        
        this.sb.listen('acc/invoice/update', function(data, self){
            var total = self.widget.get('sum');
            var val   = data.price - data.__old.price;
            self.widget.update('sum', total + val);
            self.sb.notify(total + val, 'acc/account/total/update');
        }, this);
    }
});