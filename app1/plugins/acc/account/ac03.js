/*
 * app: acc/account/data
 */
Core.register('acc03', {
    sb: null,
    widget: null,
    bl: null,
    init: function(sandbox){
        this.sb     = sandbox;
        this.widget = this.sb.getWidget('acc03');
        this.bl     = new this.block();
    },
            
    block: {
        title: 'nome',
        data:{
            'nome'  :{'name':'Nome'  , 'block':'text'     , 'req':true, 'desc': 'Digite o seu nome completo'},
            'email' :{'name':'Email' , 'block':'email'    , 'req':true, 'desc': 'Digite o email de login'},
            'senha' :{'name':'Senha' , 'block':'password' , 'req':true, 'desc': 'Digite sua senha'},
            'perfil':{'name':'Perfil', 'block':'reference', 'req':true, 'ref':{'plug':'usuario.perfil', 'type':'one'}}
        }
    },
});