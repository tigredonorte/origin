core.register('acc01', {
    sb: null,
    widget: null,
    init: function(sandbox){
        this.sb     = sandbox;
        this.widget = this.sb.getWidget('acc01');
        this.onAdd();
        this.onUpdate();
        this.onDelete();
    },
    
    onAdd: function(){
        this.sb.listen('acc/account/create', function(data, self){
            self.widget.addObject(data);
        }, this);
    },
    
    onUpdate: function(){
        this.sb.listen('acc/account/update', function(data, self){
            self.widget.addObject(data.id, data);
        }, this);
    },
    
    onDelete: function(){
        this.sb.listen('acc/account/delete', function(data, self){
            self.widget.deleteObject(data.id, data);
        }, this);
    }
});