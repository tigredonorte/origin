Plugin.usuario.login.block = function(){
    this.title   = 'nome';
    this.data    = {
        'nome'  :{'name':'Nome'  , 'block':'text'     , 'req':true, 'desc': 'Digite o seu nome completo'},
        'email' :{'name':'Email' , 'block':'email'    , 'req':true, 'desc': 'Digite o email de login'},
        'senha' :{'name':'Senha' , 'block':'password' , 'req':true, 'desc': 'Digite sua senha'},
        'perfil':{'name':'Perfil', 'block':'reference', 'req':true, 'ref':{'plug':'usuario.perfil', 'type':'one'}}
    };
};
Plugin.usuario.login.block.prototype = new Plugin.dbl();
