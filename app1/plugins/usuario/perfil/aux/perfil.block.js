Plugin.usuario.perfil.block = function(){
    this.title   = 'nome';
    this.data    = {
        'nome' :{name:'Nome'            , block:'text'      , req:true},
        'pai'  :{name:'Perfil Superior' , block:'reference' , req:false, ref:{plug:'usuario.perfil', type:'one'}},
        'desc' :{name:'Descrição'       , block:'text'      , req:true , text: {type:'paragraph'}},
        'type' :{name:'Tipo'            , block:'list'      , req:true , 'default':'sys' , options:{user:'Usuário', sys:'Sistema'}}
    };
};
Plugin.usuario.perfil.block.prototype = new Plugin.dbl();
