Plugin.dctrl = function(){
    this.sbx  = '';
    this.blk  = '';
    this.md   = '';
    this.vw   = '';
    this.link = '';
    this.block = Plugin.dbl;
    this.model = Plugin.dmd;
    this.view  = Plugin.dview;
}; 

Plugin.dctrl.prototype.init = function (sandbox){
    this.sbx  = sandbox;
    this.blk  = new this.block(sandbox);
    this.md   = new this.model(sandbox, this.blk);
    this.vw   = new this.view(sandbox, this.md, this.blk);
    this.link = this.md.getName();
};

Plugin.dctrl.prototype.index = function (){
    this.sandbox.registerView(this.link+'.index', function(){
        this.vw.index(this.md, this.blk);
    });
};

Plugin.dctrl.prototype.inserir = function (){
    this.sandbox.registerView(this.link+'.inserir', function(){
        this.vw.index(this.md, this.blk);
    }, function(dados){
       this.model.inserir(dados);
    });
};

Plugin.dctrl.prototype.editar = function (){
    this.sandbox.registerView(this.link+'.editar', function(){
        this.vw.index(this.md, this.blk);
    });
};

Plugin.dctrl.prototype.apagar = function (){
    this.sandbox.registerView(this.link+'.apagar', function(){
        this.vw.index(this.md, this.blk);
    });
};

Plugin.dctrl.prototype.show = function (){
    this.sandbox.registerView(this.link+'.show', function(){
        this.vw.index(this.md, this.blk);
    });
};