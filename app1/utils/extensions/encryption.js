/**
 * @class Utils.encryption
 */
Utils.encryption = function(key, bits){
    if(typeof bits === 'undefined') bits = 128;
    GibberishAES.size(bits);
    if(typeof key === 'undefined') key = "15Aedg9sYik3HHfh5frTT7r";
    this.key = key;
};

/**
 * @function Utils.encryption.encrypt
 * @argument {String} str A string to be encrypted
 * @returns {String} the encrypted string
 */
Utils.encryption.prototype.encrypt = function(str){
    return GibberishAES.enc(str, this.key);
};

/**
 * @function Utils.encryption.decrypt
 * @argument {String} str A string to be decrypted
 * @returns {String} the decrypted string
 */
Utils.encryption.prototype.decrypt = function(str){
    return GibberishAES.dec(str, this.key);
};