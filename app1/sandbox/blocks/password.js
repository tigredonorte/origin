/**
 * @class Blocks.password
 * @argument {object} block An object that must contain the name can contain invite option {true or false} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
Blocks.password = function(block){
    this.results    = new Core.result();
    this.name       = "Senha";
    this.bl         = new Blocks();
    this.options    = {
        minlength: {desc:'Número mínimo de caracteres que a senha deve conter (Não pode ser menor do que 6)'},
        maxlength: {desc:'Número máximo de caracteres que a senha deve conter (Não pode ser menor do que 6)'}
    };
    this.defblc = {minlength:6, maxlength:10, name: this.name};
    this.block = merge_options(this.defblc, block);
    //valida o bloco
    if(!this.bl.isValidBlock(this.block, this.name)) throw this.bl.getResponse();
    console.log(JSON.stringify(this.block));
    if(isNaN(this.block['minlength']) || this.block['minlength'] < this.defblc['minlength']){
        this.block['minlength'] = this.defblc['minlength'];
    }
    
    if(isNaN(this.block['maxlength']) || this.block['maxlength'] < this.block['minlength']){
        this.block['maxlength'] = this.block['minlength'];
    }
    //console.log(JSON.stringify(this.block));
};


/**
 * @function Blocks.password.getResponse
 * @returns {Core.result} the result of an operation @see Core.result
 */
Blocks.password.prototype.getResponse = function(){
    return this.results;
};

/**
 * @function Blocks.password.getBlock
 * @returns {Object} An object with password rules
 */
Blocks.password.prototype.getBlock = function(){
    return this.block;
};

/**
 * @function Blocks.password.getName
 * @returns {string} the name of this block
 */
Blocks.password.prototype.getName = function(){
    return this.name;
};

/**
 * @function Blocks.password.getName
 * @argument {string} value Receive a email to be validated
 * @return {Bool} true if email is valid or false otherwise
 */
Blocks.password.prototype.validate = function(value){
    this.results.reset();
    if(typeof value !== 'string'){
        this.results.setStatus(false);
        this.results.setError("A senha " + value +  " não é uma string válida!");
    }
    else if(value.length < this.block.minlength){
        this.results.setStatus(false);
        this.results.setError("A senha deve conter no mínimo " + this.block.minlength + " caracteres");
    }
    
    else if(value.length > this.block.maxlength){
        this.results.setStatus(false);
        this.results.setError("A senha deve conter no máximo " + this.block.maxlength + " caracteres");
    }
    return this.results.getStatus();
};

/**
 * @function Blocks.password.formulario
 * @argument {string} value Receive a 
 * @return {Bool} true if string is valid or false otherwise
 */
Blocks.password.prototype.formulario = function(value){
    if(!this.validate(value)) value = '';
    if(typeof value === 'undefined') value = "";
    
    var element = document.createElement("input");
    element.setAttribute('type', 'password');
    element.setAttribute('data-block', this.getName());
    element.setAttribute('data-var'  , this.block.name);
    element.value = value;
    return element;
};

Blocks.password.prototype.format = function(value){
    return value;
};

Blocks.password.prototype.onInsert = function(value){};
Blocks.password.prototype.undoInsert = function(value){};
Blocks.password.prototype.onEdit = function(value){};
Blocks.password.prototype.undoEdit = function(value){};
Blocks.password.prototype.onDelete = function(value){};
Blocks.password.prototype.undoDelete = function(value){};

