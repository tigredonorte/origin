/**
 * @class Blocks.text
 * @argument {object} block An object that must contain the name and type {line or paragrapth} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
Blocks.text = function(block){
    this.name       = "Texto";
    this.blpai      = new Blocks();
    this.block      = block;
    this.lineLenght = 64;
    this.options    = {
        type: {
            default:'line',
            options:{
                line     :{'primitive':'varchar(64)', 'name':'Uma linha de texto'},
                paragraph:{'primitive':'text', 'name':'Área de texto grande'}
             }
         }
    };
    this.results = new Core.result();
    this.defblc  = {type:'line', name:'Texto'};
    this.block   = merge_options(this.defblc, block);
    
    //valida o bloco
    if(!this.blpai.isValidBlock(this.block, this.name)) throw this.blpai.getResponse();
    if(typeof this.options.type.options[this.block.type] === 'undefined'){
        this.results.setStatus(false);
        this.results.setError("O bloco " + this.block.name + "(" + this.name + ") não possui o atributo type válido!\n\
        Os tipos válido são 'line' e 'paragraph'");
        throw this.results;
    }
};

/**
 * @function Blocks.text.getResponse
 * @returns {Core.result} the result of an operation @see Core.result
 */
Blocks.text.prototype.getResponse = function(){
    return this.results;
};

/**
 * @function Blocks.text.getName
 * @returns {string} the name of this block
 */
Blocks.text.prototype.getName = function(){
    return this.name;
};

/**
 * @function Blocks.text.validate
 * @argument {string} value Receive a string to be validated
 * @return {Bool} true if string is valid or false otherwise
 */
Blocks.text.prototype.validate = function(value){
    this.results.reset();
    //se não é uma string
    if(typeof value !== 'string') {
        this.results.setStatus(false);
        this.results.setError("A variável "+ this.block.name + " não é uma string válida!");
    }
    
    //se o atributo é do tipo linha e o tamanho da linha ultrapassou o tamanho máximo permitido
    else if(this.block.type === 'line' && value.length > this.lineLenght) {
        this.results.setStatus(false);
        this.results.setError("O atributo "+ this.block.name + " Ultrapassa o limite de "+this.lineLenght+" caracteres permitidos!");
    }
    return this.results.getStatus();
};

/**
 * @function Blocks.text.formulario
 * @argument {string} value Receive a string to be validated
 * @return {Bool} true if string is valid or false otherwise
 */
Blocks.text.prototype.formulario = function(value){
    var element;
    if(this.block.type === 'line'){
        element = document.createElement("input");
        element.setAttribute('type', 'text');
    }else if(this.block.type === 'paragraph')
        element = document.createElement("textarea");
    
    element.setAttribute('data-block', this.getName() );
    element.setAttribute('data-var'  , this.block.name);
    
    if(typeof value === 'undefined') value = "";
    element.value = value;
    return element;
};

Blocks.text.prototype.format = function(value){
    if(!this.validate(value)) return false;
    return value;
};

Blocks.text.prototype.onInsert = function(value){};
Blocks.text.prototype.undoInsert = function(value){};
Blocks.text.prototype.onEdit = function(value){};
Blocks.text.prototype.undoEdit = function(value){};
Blocks.text.prototype.onDelete = function(value){};
Blocks.text.prototype.undoDelete = function(value){};