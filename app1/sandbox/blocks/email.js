/**
 * @class Blocks.email
 * @argument {object} block An object that must contain the name can contain invite option {true or false} attributes
 * @throws {invalidBlockException} thows if block is invalid
 */
Blocks.email = function(block){
    this.definvite  = true;
    this.results    = new Core.result();
    this.name       = "Email";
    this.bl         = new Blocks();
    this.options    = {
        invite: {
            default:false,
            options:{true :{}, false:{}},
            desc:'Adicionar a esta lista e enviar convite para participar do sistema'
         }
    };
    this.defblc = {invite:true, name:'Email'};
    this.block = merge_options(this.defblc, block);
    
    //valida o bloco
    if(!this.bl.isValidBlock(this.block, this.name)) throw this.bl.getResponse();
};



/**
 * @function Blocks.email.getResponse
 * @returns {Core.result} the result of an operation @see Core.result
 */
Blocks.email.prototype.getResponse = function(){
    return this.results;
};

/**
 * @function Blocks.email.getName
 * @returns {string} the name of this block
 */
Blocks.email.prototype.getName = function(){
    return this.name;
};

/**
 * @function Blocks.email.getName
 * @argument {string} value Receive a email to be validated
 * @return {Bool} true if email is valid or false otherwise
 */
Blocks.email.prototype.validate = function(value){
    this.results.reset();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(value)) {
        this.results.setStatus(false);
        this.results.setError("O Email " + value +  " é inválido!");
    }
    return this.results.getStatus();
};

/**
 * @function Blocks.email.formulario
 * @argument {string} value Receive a 
 * @return {Bool} true if string is valid or false otherwise
 */
Blocks.email.prototype.formulario = function(value){
    if(!this.validate(value)) return false;
    if(typeof value === 'undefined') value = "";
    
    var element = document.createElement("input");
    element.setAttribute('type', 'text');
    element.setAttribute('data-block', this.getName());
    element.setAttribute('data-var'  , this.block.name);
    element.value = value;
    return element;
};

Blocks.email.prototype.format = function(value){
    if(!this.validate(value)) return false;
    return value;
};

Blocks.email.prototype.onInsert = function(value){};
Blocks.email.prototype.undoInsert = function(value){};
Blocks.email.prototype.onEdit = function(value){};
Blocks.email.prototype.undoEdit = function(value){};
Blocks.email.prototype.onDelete = function(value){};
Blocks.email.prototype.undoDelete = function(value){};

