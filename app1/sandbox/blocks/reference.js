Blocks.reference = function(){
    this.options = {
        plug: {desc:'Escolha o aplicativo referenciado'},
        type: {
            default:'one',
            options:{
                one  :{name:'Simples'},
                multi:{name:'Múltipla'}
             },
             desc:'Existem dois tipos de referência: \n\
                   A simples permite que apenas um item do aplicativo seja referenciado\n\
                   A múltipla permite que mais de um item do aplicativo seja referenciado.'
         }
    };
};
