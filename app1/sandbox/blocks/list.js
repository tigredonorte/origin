Blocks.list = function(){
    this.options = {
        'choices_possible': {
            'default':false,
            'options':{
                one     :{name: 'Apenas uma escolha'},
                limited :{name: 'Número limitado de escolhas'},
                ilimited:{name: 'Sem limite de escolhas'}
            },
            'desc':'Número de escolhas possíveis'
         }
    };
};
