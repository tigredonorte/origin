function Blocks(){
    this.results = new Core.result();
}

Blocks.prototype.getResponse = function(){
    return this.results;
}

Blocks.prototype.isValidBlock = function(block, blockname){
    this.results.reset();
    try{
        if(!this.proprietyExists(block, 'name'));
        else if(block.name === ""){
            this.results.setStatus(false);
            this.results.setError("O bloco " + blockname + " deve ser preenchido!");
        }
    }catch(e){
        this.results.setStatus(false);
        this.results.setError("O bloco " + blockname + " foi inicializado incorretamente!");
    }
    return this.results.getStatus();
}

Blocks.prototype.proprietyExists = function(bl, propriety){
    this.results.reset();
    if(typeof bl[propriety] === 'undefined'){
        if(typeof defaultt === 'undefined') {
            this.results.setStatus(false);
            this.results.setError("O bloco não possui o atributo " + propriety);
        }
    }
    return this.results.getStatus();
}