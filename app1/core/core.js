function Core(){
    //"use strict";
    this.instances = {};
    this.sbox = new Sandbox();
}

Core.prototype.register = function(plugin, action) {
    if(typeof action !== 'function') return false;
    if(!method_exists (action, 'init')){
        var obj = new action();
        if(!method_exists(obj, 'init')){
            this.log.log("O plugin "+plugin+" não possui o método init");
            //console.log("O plugin "+plugin+" não possui o método init");
            return false;
        }else{
            obj.init(this.sbox);
        }
    }
    
    //console.log(plugin + " Registrado! ");
    this.instances[plugin] = action;
    return true;
};

Core.prototype.createInstance = function(plugin) {
    try{
        var cls = this.instances[plugin];
        if(typeof cls !== 'function'){
            //console.log("O plugin "+plugin+" não foi registrado como objeto corretamente!");
            return null;
        }
        var obj = new cls();
        
        obj.init(this.sbox);
        console.log("d");
        return obj;
    }catch(e){
        console.log(JSON.stringify(e));
        return null;
    }
};

Core.prototype.init = function() {
    alert(JSON.stringify(Plugin));
};