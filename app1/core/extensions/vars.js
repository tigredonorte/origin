Core.vars = function(){
    this.urls = window.location.protocol+"//" +window.location.host;
    this.curl = this.urls + '/' + window.location.pathname;
};

Core.vars.prototype.getUrl = function(){
    return this.urls;
}

Core.vars.prototype.getCurrentUrl = function(){
    return this.curl;
}